const fs = require('fs');
const args = require('command-line-args');
const store = require('./store.js');
const API = require('../../lib/api/api.js');

const optionDefinitions = [
    { name: 'file', alias: 'f', type: String },
    { name: 'user', alias: 'u', type: String }
]
const options = args(optionDefinitions);
// ----------------------------------------------------------------------------------------
var userObj=null;
var fileObj=null;
var emailObj=null;
var api = new API();
console.log(`...getting user account information`);
api.request.get("/users/me").then((response)=>{
    userObj=response.data;
    console.log(`...sender user is ${userObj.name} (${userObj.email})`);
    console.log(`...uploading file to folder id ${userObj.mydirId}`);
    api.request.upload(`/folders/${userObj.mydirId}/actions/file?returnEntity=true`, options.file).then((response)=>{
        fileObj=response.data;
        console.log(`...file uploaded with id ${fileObj.id}`);
        console.log(`...sending email to approver ${options.user}`);
        var formData = {
            to:[options.user],
            files: [fileObj.id],
            type: "original",
            subject: `APPROVAL REQUEST [${fileObj.id}] [${fileObj.fingerprint}] (${fileObj.name})`,
            body: `Please review the enclosed document ${fileObj.name}.\n\nTo approve, please reply to this email with the text "APPROVE" in the message body, to reject, please type "REJECT" in your reply.`,
        };
        api.request.post("/mail/actions/sendFile?returnEntity=true",formData).then((response)=>{
            emailObj = response.data;
            (async () => {
                var result = await store.run("INSERT INTO approvals (approver,emailId,fileId,fingerprint,date) VALUES(?,?,?,?,?)",[
                    options.user,
                    emailObj.id,
                    fileObj.id,
                    fileObj.fingerprint,
                    Date.now()
                ]);
            })();
            console.log(`done`);
        },errorHandler);        
    },errorHandler);
},errorHandler);

// ----------------------------------------------------------------------------------------
function errorHandler(err) {
    console.log(`${err}`);
    process.exit(1);
}
// ----------------------------------------------------------------------------------------
