const axios = require('axios').create({timeout: 15000, maxBodyLength: 1000000000});
const FormData = require('form-data');
const fs = require('fs');
const path = require('path');

class EPIC_API_REQUEST {
    // ---------------------------------------------------------------------------------
    constructor(EPIC_API) {
        this.EPIC_API=EPIC_API;
        this.__cancelledUploads=[];
        this.__baseUrl="https://fhir.epic.com/interconnect-fhir-oauth/API";
    }
    // ---------------------------------------------------------------------------------
    get (endpoint) {
        return this.__baseRequest("get",endpoint);
    }
    // ---------------------------------------------------------------------------------
    delete (endpoint) {
        return this.__baseRequest("delete",endpoint);
    }
    // ---------------------------------------------------------------------------------
    head (endpoint) {
        return this.__baseRequest("head",endpoint);
    }
    // ---------------------------------------------------------------------------------
    options (endpoint) {
        return this.__baseRequest("options",endpoint);
    }
    // ---------------------------------------------------------------------------------
    post (endpoint,data) {
        return this.__baseRequest("post",endpoint,data);
    }
    // ---------------------------------------------------------------------------------
    patch (endpoint,data) {
        return this.__baseRequest("patch",endpoint,data);
    }
    // ---------------------------------------------------------------------------------
    put (endpoint,data) {
        return this.__baseRequest("put",endpoint,data);
    }
    // ---------------------------------------------------------------------------------
    download (endpoint,file,folder) {
        return new Promise((resolve,reject)=>{
            this.EPIC_API.token.get().then((token)=>{
                (async ()=>{
                    var response = await axios({
                    method: "get",
                    url: this.__baseUrl+endpoint,
                    headers: this.__headers('get',token),
                    responseType: 'stream'
                });
                const writer = fs.createWriteStream(path.join(folder,file.name));
                response.data.pipe(writer); 
                writer.on('finish', resolve);
                writer.on('error', reject);            
            })();
            },(err)=>{return reject(err);})
        });
    }    
    // ---------------------------------------------------------------------------------
    upload(endpoint,file) {
        return new Promise((resolve,reject)=>{
            this.EPIC_API.token.get().then((token)=>{
                var formData = new FormData();
                formData.append("body", fs.createReadStream(file),{filename: path.basename(file)});
                var headers = this.__headers("post",token);
                headers['Content-Type'] = 'multipart/form-data; boundary='+formData.getBoundary();
                headers['Accept-Encoding'] = 'gzip, deflate, br';
                headers['Connection'] = 'keep-alive';

                axios({
                    method: "post",
                    url: his.__baseUrl+endpoint,
                    headers: headers,
                    data: formData
                }).then((response) => {
                    return resolve(response);
                }).catch((error) => {
                    return reject(error);
                });        
            },(err)=>{return reject(err);});
        });
    }    
    // ---------------------------------------------------------------------------------
    uploadChunks(folderId, file, chunkSize, progress) {
        return new Promise ((resolve,reject)=>{
            if (!fs.existsSync(file)) return reject(`file does not exist: ${file}`);
            this.EPIC_API.token.get().then((token)=>{  
                var filename    = path.basename(file);
                var stats       = fs.statSync(file);
                var totalSize   = stats["size"];
                var totalChunks = Math.ceil(totalSize / (chunkSize*1024));
                var form        = {filename:filename, totalChunks:totalChunks, totalSize:totalSize};
                console.log(`upload for ${file} will have ${totalChunks} chunks`);
                if (this.__checkCancelled(file)) return resolve("cancelled by user");
                this.post(`/folders/${folderId}/actions/initiateUpload?returnEntity=true`,form).then((response)=>{
                    if (response.errors) return reject(response.errors);
                    console.log(`initiated upload for ${file}`);
                    if (this.__checkCancelled(file)) return resolve("cancelled by user");
                    this.get(`/uploads/${response.data.id}?returnEntity=true`).then((response)=>{
                    if (response.errors) return reject(response.errors);
                    var uri = response.data.uri;
                    console.log(`got upload uri for ${file}: ${uri}`);
                    if (this.__checkCancelled(file)) return resolve("cancelled by user");
                    this.__handleChunks(uri,file,totalChunks,chunkSize,progress).then((response)=>{
                        console.log(`file uploaded: ${file}`);
                        return resolve(response);
                    },(err)=>{
                        return reject(err);
                    });
                    },(err)=>{
                    return reject(err);
                    });
                },(err)=>{
                    return reject(err);
                });
            },(err)=>{
                return reject(err);
            });
        });
      }
    // ---------------------------------------------------------------------------------
    cancelUpload (file) {
        this.__cancelledUploads.push(file);
    }      
    // ---------------------------------------------------------------------------------
    __handleChunks (uri,file,total,chunkSize,progress) {
        var self=this;
        return new Promise ((resolve,reject)=>{
          var chunkIndex = 1;
          var reader = fs.createReadStream(file, { highWaterMark: (chunkSize*1024) });
          reader.on('data', function (chunk) {
            reader.pause();
            var size = Buffer.byteLength(chunk);
            var form = {
              content: chunk,
              index: chunkIndex,
              compressionMode: "NORMAL",
              compressionSize: size,
              originalSize: size
            };
            console.log(`uploading chunk ${chunkIndex} for file ${file} of size ${size}`);
            if (self.__checkCancelled(file)) return resolve("cancelled by user");
            self.__processChunk("/"+uri+"?returnEntity=true",form).then((response)=>{
              if (response.errors) return reject(response.errors);
              chunkIndex++;
              if (progress) progress(Number(100 * (chunkIndex - 1) / total).toFixed(2));
              if (chunkIndex-1==total) {
                reader.close();
                return resolve(response);
              }
              reader.resume();
            },(err)=>{
              return reject(err);
            });
          });
        });
      }    
    // ---------------------------------------------------------------------------------
    __checkCancelled (file) {
        var self = this;
        if (self.__cancelledUploads.indexOf(file)>=0) {
            self.__cancelledUploads=self.__cancelledUploads.filter((f)=>{return f!=file});
            return true;
        } else {
            return false;
        }
    }    
    // ---------------------------------------------------------------------------------
    __processChunk(endpoint, form) {
        return new Promise ((resolve,reject)=>{
          try {
            this.EPIC_API.token.get().then((token)=>{    
                var data = "";

                var formData = new FormData();
                for (var i = 0; i < Object.keys(form).length; i++) {formData.append(Object.keys(form)[i],form[Object.keys(form)[i]]);}

                var headers = this.__headers("post",token);
                headers['Content-Type'] = 'multipart/form-data; boundary='+formData.getBoundary();
                headers['Accept-Encoding'] = 'gzip, deflate, br';
                headers['Connection'] = 'keep-alive';
                headers['Content-Length'] = 'keep-alive'+formData.getLengthSync();
                headers['Transfer-Encoding'] = 'chunked';

                axios({
                    method: "post",
                    url: his.__baseUrl+endpoint,
                    headers: headers,
                    data: formData
                }).then((response) => {
                    return resolve(response);
                }).catch((error) => {
                    return reject(error);
                });                 
            },(err)=>{
                return reject(err);
            });                
          } catch (e) {
            return reject(e);
          }
        });
    }    
    // ---------------------------------------------------------------------------------
    __baseRequest(method,endpoint,data) {
        return new Promise((resolve,reject)=>{
            //console.log(this.__baseUrl+endpoint)
            this.EPIC_API.token.get().then((token)=>{
                axios({
                    method: method,
                    url: this.__baseUrl+endpoint,
                    headers: this.__headers(method,token,data),
                    data: data,
                }).then((response) => {
                    return resolve(response);
                }).catch((error) => {
                    return reject(error);
                });        
            },(err)=>{return reject(err);})
        });
    }    
    // ---------------------------------------------------------------------------------
    __headers(method,token,data) {
        var headers = {
            "Authorization":`Bearer ${token.access_token}`,
            //"accept": "application/json",
        };
        return headers;
    }
    // ---------------------------------------------------------------------------------
    __query(options) {
        var __query = "";
        if (options) for (var key in options) { if (__query != "") __query += "&"; __query += key + "=" + encodeURIComponent(options[key]); }
        if (__query!="") __query = "?"+ __query;
        return __query;
      }
    // ---------------------------------------------------------------------------------
    
}
module.exports=EPIC_API_REQUEST;