const keytar = require('keytar');
class EPIC_API_TOKEN {
    // -------------------------------------------------------------------------------------
    __token=null;
    __name=null;
    __service="EPIC";
    __name="ACCESS_TOKEN";
    // -------------------------------------------------------------------------------------
    constructor(EPIC_API) {
        this.EPIC_API=EPIC_API;
     }
    // -------------------------------------------------------------------------------------
    load () {
        //console.log(`using epic token ${this.__name}`);
        return new Promise((resolve, reject) => {
            keytar.getPassword(this.__service, this.__name).then((obj)=>{
                try {
                    this.__token = JSON.parse(obj);
                    if (this.__token && this.__token.access_token) return resolve(this.__token);
                    else return reject(`epic token not found: ${this.__name}`);
                } catch (e) {
                    return reject(`unable to parse epic token: ${this.__name}`);
                }
            }, (err)=>{
                return reject(`epic token load error: ${this.__name}`,);
            });
        });
    }
    // -------------------------------------------------------------------------------------
    get () {
        return new Promise((resolve, reject) => {
            if (this.__token) return resolve(this.__token);
            this.load().then((token)=>{return resolve(token)},(err)=>{return reject(err);});
        });
    }
    // -------------------------------------------------------------------------------------
    set (token) {
        this.__token=token;
    }
    // -------------------------------------------------------------------------------------
    clear() {
        this.__token=null;
    }
    // -------------------------------------------------------------------------------------
}
module.exports=EPIC_API_TOKEN;
