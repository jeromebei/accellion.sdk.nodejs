# Epic

## Description

Basic example to demonstrate EPIC use cases (https://fhir.epic.com/)

## login.js
Logs you into the Epic application.

You will need to create a developer account on Epic (https://fhir.epic.com/), create a custom application and retrieve the development client id. 

Add the client id to login.js (see source code comments) by replacing the following line:

~~~javascript
	const clientId = require("./auth.json").clientId;
~~~   

with 

~~~javascript
	const clientId = "<your development client id>";
~~~   

This code will open up a browser window, allowing you to log in with your Epic user account. It will then retrieve an access token, and securely store it in your credential vault.

### Usage
~~~bash
    node login.js
~~~    

## patient.js

Loads patient data in JSON format from an array of patient ID's and stores the data in kiteworks (current user's "MyFolder")

### Usage
~~~bash
    node patient.js
~~~    
