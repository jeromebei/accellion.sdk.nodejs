const express = require('express');
const https = require('https');
const path = require('path');
const open = require('open');
const keytar = require('keytar');


const LOCALPORT    = 8989; // set this to a free port on your local computer
const clientId     = require("./auth.json").clientId; // set this to your prod or dev client id in your epic custom application

const EPIC = {
    PORT          : 443,
    PROTOCOL      : "https",
    SERVER        : "fhir.epic.com",
    BASE_URI      : "/interconnect-fhir-oauth",
    AUD           : "https://fhir.epic.com/interconnect-fhir-oauth/api/fhir/dstu2",
    CLIENT_ID     : clientId,
    REDIRECT_URI  : `http://localhost:${LOCALPORT}`,
}

const TOKENSERVICE = "EPIC";
const TOKENNAME    = "ACCESS_TOKEN";
var app = express();
app.get('/login', function (req, res) {
  var authUri = `${EPIC.PROTOCOL}://${EPIC.SERVER}${EPIC.BASE_URI}/oauth2/authorize?client_id=${EPIC.CLIENT_ID}&response_type=code&redirect_uri=${encodeURIComponent(EPIC.REDIRECT_URI)}&aud=${encodeURIComponent(EPIC.AUD)}`;
  res.redirect(authUri);
});
app.get('/', function (req, res) {
  var code = req.query.code;
  var formData = `grant_type=authorization_code&code=${req.query.code}&redirect_uri=${encodeURIComponent(EPIC.REDIRECT_URI)}&client_id=${EPIC.CLIENT_ID}`;

  var options = {
    hostname: EPIC.SERVER,
    port:     EPIC.PORT,
    path:     `${EPIC.BASE_URI}/oauth2/token`,
    method:   'POST',
    headers:  {'Content-Type': 'application/x-www-form-urlencoded','Content-Length': formData.length}
  };
  const req2 = https.request(options, res2 => {
    res2.on('data', d => {
      var tokenData = JSON.parse(d.toString('utf8'));

      keytar.setPassword(TOKENSERVICE,TOKENNAME,JSON.stringify(tokenData)).then(()=>{
          console.log(`Token stored in keychain ${TOKENSERVICE}, account: ${TOKENNAME}`);
          console.log(`All done.`);
          console.log("");
          res.send("Token retrieved and stored. You can now close this browser window.");
          process.exit(0);
          });
    });
  });
  req2.on('error', (e) => {
    console.error(e);
  });
  req2.write(formData);
  req2.end();
});
  
  
app.listen(LOCALPORT, function () {
  console.log(`Please open a web browser with the following URL:`);
  console.log("");
  console.log(`   http://localhost:${LOCALPORT}/login`);
  console.log("");
  console.log("Log in with your Epic user account.");
  console.log("");
  open(`http://localhost:${LOCALPORT}/login`);
});
