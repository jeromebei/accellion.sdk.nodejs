const EPIC_API = require('./epic.api.js');
const API      = require('../../lib/api/api.js');
const epicApi = new EPIC_API();
const api = new API();

var ids=["erXuFYUfucBZaryVksYEcMg3","eq081-VQEgP8drUUqCWzHfw3","eAB3mDIBBcyUKviyzrxsnAw3"]; // for more test patients, check: https://fhir.epic.com/Documentation?docId=testpatients

function upload(idx,url,done){
    if (idx >= ids.length) return done();
    var id = ids[idx];        
    epicApi.request.get(`/FHIR/DSTU2/Patient/${id}`).then((response) => {
        console.log(`... found patient ${response.data.name[0].text}`)
        var patientData = JSON.stringify(response.data,null,3);
        //console.log(patientData);
        api.request.uploadRaw(url,patientData,id+".json").then((response2)=>{
            console.log(`... uploaded patient ${response.data.name[0].text}`)
            idx++;
            upload(idx,url,done);
        });
    });
}

api.request.get("/users/me").then((response) => {
    var myFolderId=response.data.syncdirId;
    //console.log(`found "My Folder" id: ${myFolderId}`);

    upload(0,`/folders/${myFolderId}/actions/file?returnEntity=true`,()=>{
        console.log("all done.");
    });
});
