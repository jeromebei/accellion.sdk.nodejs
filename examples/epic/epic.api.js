class EPIC_API {
    // -------------------------------------------------------------------------------------
    CACHE = new (require("../../lib/cache.js"))();
    // -------------------------------------------------------------------------------------
    constructor() {
        this.request = new (require ("./epic.api.request.js"))(this);
        this.token = new (require ("./epic.api.token.js"))(this);
    }    
    // -------------------------------------------------------------------------------------
}

module.exports = EPIC_API;