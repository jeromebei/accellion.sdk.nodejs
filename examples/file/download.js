const API = require('../../lib/api/api.js');
const fs = require('fs');

const api = new API();
//get the users' "My Folder" id
api.request.get("/users/me").then((response) => {
    var myFolderId=response.data.syncdirId;
    var userEmail=response.data.email;
    console.log(`found "My Folder" id: ${myFolderId} for ${userEmail}`);
    //list all files in "My Folder"
    api.request.get(`/folders/${myFolderId}/files`).then((response) => {
        var files=response.data.data;
        console.log(`found ${files.length} files in ${myFolderId}`);
        if (files.length==0) {
            console.log(`This example needs at least 1 file to be present in the user's "My Folder"`);
            return;
        }
        //we'll only download the first file
        var file = files[0];
        api.request.download(`/files/${file.id}/content`,file,".").then(()=>{
            console.log(`file ${file.name} downloaded`);
        });

    });    
});
