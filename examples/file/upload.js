const API = require('../../lib/api/api.js');
const fs = require('fs');

const api = new API();
//get the users' "My Folder" id
api.request.get("/users/me").then((response) => {
    var myFolderId=response.data.syncdirId;
    var userEmail=response.data.email;
    console.log(`found "My Folder" id: ${myFolderId} for ${userEmail}`);
    api.request.upload(`/folders/${myFolderId}/actions/file?returnEntity=true`, "./report.pdf").then((response)=>{
        console.log("report.pdf uploaded into the user's 'My Folder'.")
    });
});
