# File 

### Description

Demonstrates some of the Accellion API /files and /folders endpoints.

## download.js
Downloads the first file found in the current user's "My Folder". At least one file needs to be present in the folder for theis example to work. The file will be downloaded into the working directory.

### Usage
    node download.js
    
## upload.js
Uploads the file report.pdf into current user's "My Folder".

### Usage
    node upload.js
