const fs = require('fs');
const API = require('../../lib/api/api.js');

const api = new API();
const baseFolderId="b6cbd49f-02ba-4a5c-a533-8fc3c6a631f5";
const userRole = 2; //Downloader

api.token.test("*default*").then((token)=>{
    fs.readFile("users.csv", 'utf8', function(err, data) {
        if (err) return console.error('import file not found');
        data = data.split("\n");
        createUsers(data,()=>{
            return console.log('all done');
        });
    });

},(err)=>{
    console.error('token not found');
});

function createUsers(data,done,idx) {
    if (!idx) idx = 0;
    if (idx >= data.length) return done();
    if (data[idx].trim()==="") {idx++; return createUsers(data,done,idx);}
    row=data[idx].split(",");
    console.log(`creating user: ${row[0]}`);
    var form = {
        "name":row[0],
        "email":row[1],
        "password":row[2],
        "userTypeId":1,
        "verified":true,
        "sendNotification": false
    };
    api.request.post("/users?returnEntity=true",form).then((response) => {
        var userId=response.data.id;
        console.log(`user created: ${row[0]}, id: ${userId}`);
        form = {"name":row[3]};
        api.request.post(`/folders/${baseFolderId}/folders?returnEntity=true`,form).then((response) => {
            var folderId=response.data.id;
            console.log(`folder created: ${row[3]}, id: ${folderId}`);
            form = {notify: false,userIds:[""+userId], roleId:userRole};
            api.request.post(`/folders/${folderId}/members?returnEntity=true`,form).then((response) => {
                var folderId=response.data.id;
                console.log(`user added fo folder`);
                idx++;
                return createUsers(data,done,idx);
            },(err)=>{
                return console.error(`${err.response.status}: ${err.response.statusText} -> ${err.response.config.data}`);
            });  
        },(err)=>{
            return console.error(`${err.response.status}: ${err.response.statusText} -> ${err.response.config.data}`);
        });  
    },(err)=>{
        return console.error(`${err.response.status}: ${err.response.statusText} -> ${err.response.config.data}`);
    });  
}
