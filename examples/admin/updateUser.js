const API = require('../../lib/api/api.js');

const api = new API();
var form = {
    "name":"Test User",
    "email":"testuser@somewhere.com",
    "password":"In1tialP@$$w0rd",
    "userTypeId":1,
    "verified":true,
    "sendNotification": false
};
api.request.post("/users?returnEntity=true",form).then((response) => {
    var userId=response.data.id;
    form = {
        "password":"00SomePassw!!rD8",
        "name": "Jerome Bei",
        "suspended": false,
        "verified": true
    };
        
    api.request.put(`/admin/users/${userId}`,form).then((response) => {
        return console.log(response.data);
    });
});
