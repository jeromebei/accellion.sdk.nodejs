# Activities

## Description

Basic example to demonstrate the usage of the /activities endpoint.

## list.js
Returns a JSON array containing a list of recent activities for the current Accellion user.

### Usage
~~~bash
    node list.js
~~~    

## diff.js
Looks at the last day's upload and download activities, reports uploaded files that haven't been downloaded based on file id.

### Usage
~~~bash
    node diff.js
~~~    