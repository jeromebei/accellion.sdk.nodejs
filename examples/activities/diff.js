const API = require('../../lib/api/api.js');

const api = new API();
const rootFolder = "My Folder/ACT Test";
api.request.get("/activities?type=file_changes&noDayBack=1").then((response) => {
    var activities    = response.data.data;
    activities = activities.filter((a)=>{return a.data && a.data.file && a.data.file.path.startsWith(rootFolder)});
    var uploads       = activities.filter((a)=>{return a.event=="add_file"});
    var downloads     = activities.filter((a)=>{return a.event=="download"});
    var notDownloaded = uploads.filter((u)=>{return !downloads.map((d)=>{return d.data.file.id}).includes(u.data.file.id);});
    
    console.log(`uploads: ${uploads.length}, downloads: ${downloads.length}`);
    console.log(`not downloaded: ${notDownloaded.length} files =>`);
    console.log(notDownloaded.map((d)=>{return `${d.data.file.path}/${d.data.file.name} => uploaded on: ${d.created}`;}).join("\n"));
});
