# Email

## Description

Basic example to demonstrate the usage of the /mail endpoint.

## list.js
Returns a JSON array containing a list of unread emails.

### Usage
~~~bash
    node list.js
~~~    