var StateManager={
  //============================================================================
  __states: [],
  __queue:[],
  STATE: {"RUNNING":1, "IDLE":2, "ERROR":3, "UNKNOWN":4},
  WAIT_TIME: 50,
  //============================================================================
  get: (name)=>{
    var __instance = StateManager.__states.find((el)=>{return el.name==name});
    if (!__instance) {
      __instance = {name:name,state:StateManager.STATE.UNKNOWN};
      StateManager.__states.push(__instance);
    }
    return __instance;
  },
  //============================================================================
  set: (name,state)=>{
    var __instance = StateManager.get(name);
    __instance.state=state;
    return __instance;
  },
  //============================================================================
  waitForIdle: (name,done)=>{
    var q=StateManager.__queue.find((el)=>{return el.name==name});
    if (!q) {q={name:name,queue:[done]};StateManager.__queue.push(q);}
    else q.queue.push(done);

    (function wfi(name, done) {
      var q=StateManager.__queue.find((el)=>{return el.name==name});
      if (q && StateManager.isIdle(name)||StateManager.isUnknown(name)) return q.queue.shift()();
      setTimeout(()=>{wfi(name,done);},StateManager.WAIT_TIME);
    })(name,done);
  },
  //============================================================================
  setRunning:(name)=>{return StateManager.set(name,StateManager.STATE.RUNNING);},
  setIdle:(name)=>{return StateManager.set(name,StateManager.STATE.IDLE);},
  setError:(name)=>{return StateManager.set(name,StateManager.STATE.ERROR);},
  setUnknown:(name)=>{return StateManager.set(name,StateManager.STATE.UNKNOWN);},
  //============================================================================
  isRunning:(name)=>{return StateManager.get(name).state==StateManager.STATE.RUNNING;},
  isIdle:(name)=>{return StateManager.get(name).state==StateManager.STATE.IDLE;},
  isError:(name)=>{return StateManager.get(name).state==StateManager.STATE.ERROR;},
  isUnknown:(name)=>{return StateManager.get(name).state==StateManager.STATE.UNKNOWN;},
  //============================================================================
};
module.exports=StateManager;
