//==============================================================================
var Threads = {
  STATE:{"STOPPED":1, "RUNNING":2, "DONE":3,"ERROR":4},
  VERBOSE: false,
  //============================================================================
  __pools : [],
  //============================================================================
  createPool:(name,maxThreads)=>{
    if (Threads.VERBOSE) console.log (`Threads: creating thread pool ${name}`);
    var pool = {
      state: Threads.STATE.STOPPED,
      name:name,
      maxThreads:maxThreads,
      cb:null,
      queue:[],
      __idCount:0,
      __watchInterval: 150,
      __stopping: false,
      //------------------------------------------------------------------------
      add: (fn,context)=>{Threads.addThread(name,fn,context)},
      remove: (id)=>{Threads.removeThread(name,id)},
      run: (cb)=>{return Threads.runPool(name,cb)},
      stop: ()=>{return Threads.stopPool(name)},
      running: ()=>{return Threads.getPool(name).queue.filter((t)=>{return t.state==Threads.STATE.RUNNING;}).length},
      remaining: ()=>{return Threads.getPool(name).queue.filter((t)=>{return t.state==Threads.STATE.STOPPED;}).length},
      setSize: (maxThreads)=>{return Threads.setSize(name,maxThreads)},
      //------------------------------------------------------------------------
    };
    if (Threads.getPool(name)) {Threads.stopPool(name); Threads.deletePool(name);}
    Threads.__pools.push(pool);
    return pool;
  },
  //============================================================================
  deletePool:(name)=>{
    if (Threads.VERBOSE) console.log (`Threads: deleting thread pool ${name}`);
    Threads.__pools=Threads.__pools.filter((n)=>{return n.name!=name});
  },
  //============================================================================
  setSize:(name, maxThreads)=>{
    var pool = Threads.getPool(name);
    if (pool) pool.maxThreads=maxThreads;
    if (Threads.VERBOSE) console.log ("Threads: pool "+pool.name+" changed size to "+pool.maxThreads);

    return pool;
  },
  //============================================================================
  getPool:(name)=>{
    return Threads.__pools.find((n)=>{return n.name==name});
  },
  //============================================================================
  addThread:(name,fn,context)=>{
    if (Threads.VERBOSE) console.log (`Threads: adding thread to pool ${name}`);
    var pool = Threads.getPool(name);
    if (pool) {
      thread = new Thread(fn,context,pool.__idCount);
      pool.queue.push(thread);
      pool.__idCount++;
      return thread.id;
    } else {
      return -1;
    }
  },
  //============================================================================
  removeThread:(name,id)=>{
    if (Threads.VERBOSE) console.log (`Threads: removing thread ${id} from pool ${name}`);
    var pool = Threads.getPool(name);
    if (pool) pool=pool.filter((n)=>{return n.id!=id});
  },
  //============================================================================
  runPool:(name,cb)=>{
    if (Threads.VERBOSE) console.log (`Threads: running thread to pool ${name}`);
    var pool = Threads.getPool(name);
    if (!pool) return cb({error:"pool not found"});
    if (Threads.VERBOSE) console.log ("running pool "+pool.name+" with size "+pool.maxThreads);
    pool.state=Threads.STATE.RUNNING;
    pool.__stopping=false;
    pool.cb=cb;
    (function __run(){

      while (pool.running() < pool.maxThreads && pool.remaining() >0) {
        nextThread = pool.queue.find((t)=>{return t.state==Threads.STATE.STOPPED});
        nextThread.state=Threads.STATE.RUNNING;
        if (Threads.VERBOSE) console.log (`Threads: running thread ${nextThread.id} in pool ${name}`);
        nextThread.fn(nextThread,nextThread.context,(thread,err)=>{
          if (Threads.VERBOSE) console.log (`Threads: finished thread ${thread.id} in pool ${name}`);
          if (err) thread.state=Threads.STATE.ERROR;
          else thread.state=Threads.STATE.DONE;
          pool.queue=pool.queue.filter((t)=>{return t.id!=thread.id});
          if (pool.queue.length==0) pool.stop();
        });
      }
      if (!pool.__stopping) setTimeout(__run,pool.__watchInterval);
      else pool.__stopping=false;
    })();
    return pool;
  },
  //============================================================================
  stopPool:(name)=>{
    var pool = Threads.getPool(name);
    pool.state=Threads.STATE.STOPPED;
    if (pool.cb) pool.cb(pool);
    pool.__stopping=true;
    return pool;
  },
  //============================================================================
  stopAllPools:()=>{
    for (var i = 0; i < Threads.__pools.length; i++) {
      Threads.__pools[i].stop();
    }
  },
  //============================================================================
}
//==============================================================================
class Thread {
  //============================================================================
  constructor(fn,context,id) {
    this.state=Threads.STATE.STOPPED;
    this.fn=fn;
    this.context=context;
    this.id=id;
  }
  //============================================================================
}

//==============================================================================
module.exports=Threads;
