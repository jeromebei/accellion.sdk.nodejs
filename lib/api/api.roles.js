class API_ROLES {
    // -------------------------------------------------------------------------------------
    constructor(API) {
        this.API=API;
     }
    // -------------------------------------------------------------------------------------
    list (invalidateCache) {
        return new Promise((resolve, reject) =>{
            if (invalidateCache) this.API.CACHE.clear("ROLES");
            if (this.API.CACHE.all("ROLES").length>0) return resolve(this.API.CACHE.all("ROLES").map(el=>{return el.value}));
            this.API.request.get("/roles").then((response)=>{
                if (!response.data || !response.data.data) return reject("no data");
                response.data.data.forEach((dataset)=>{this.API.CACHE.set("ROLES",dataset.id,dataset);});
                return resolve(this.API.CACHE.all("ROLES").map(el=>{return el.value}));
            },(err)=>{
                return reject(err);
            });
        });
      }    
    // -------------------------------------------------------------------------------------
}
module.exports=API_ROLES;
