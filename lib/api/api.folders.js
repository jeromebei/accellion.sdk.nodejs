class API_FOLDERS {
    // -------------------------------------------------------------------------------------
    constructor(API) {
        this.API=API;
     }
    // -------------------------------------------------------------------------------------
    list (id,options) {
      return new Promise((resolve, reject) =>{
        var endpoint = "/folders/top";
        if (id) endpoint="/folders/"+id+"/children";
        var query = this.API.request.__query(options);
        this.API.request.get(endpoint + query,this.API.request.__query(options)).then((response)=>{
          if (!response.data || !response.data.data) return reject("no data");
          return resolve(response.data.data);
        },(err)=>{
            return reject(err);
        });
      });
    }
    // -------------------------------------------------------------------------------------
}
module.exports=API_FOLDERS;
