const keytar = require('keytar');
class API_TOKEN {
    // -------------------------------------------------------------------------------------
    __token=null;
    __name=null;
    __service="accellion.token";
    // -------------------------------------------------------------------------------------
    constructor(API,tokenName) {
        this.API=API;
        this.__name = tokenName ? tokenName : "*default*";
     }
    // -------------------------------------------------------------------------------------
    load () {
        //console.log(`using token ${this.__name}`);
        return new Promise((resolve, reject) => {
            keytar.getPassword(this.__service, this.__name).then((obj)=>{
                try {
                    this.__token = JSON.parse(obj);
                    if (this.__token && this.__token.access_token) return resolve(this.__token);
                    else return reject(`token not found: ${this.__name}`);
                } catch (e) {
                    return reject(`unable to parse token: ${this.__name}`);
                }
            }, (err)=>{
                return reject(`token load error: ${this.__name}`,);
            });
        });
    }
    // -------------------------------------------------------------------------------------
    test (name) {
        return new Promise((resolve, reject) => {
            var oldname=this.__name;
            var oldtoken=this.__token;
            this.__name=name;
            this.load().then((token)=>{
                this.__name=oldname;
                this.__token=oldtoken;
                return resolve(this.__token);
            },(err)=>{
                this.__name=oldname;
                this.__token=oldtoken;
                return reject("token not found");
            });
        });
    }
    // -------------------------------------------------------------------------------------
    get () {
        return new Promise((resolve, reject) => {
            if (this.__token) return resolve(this.__token);
            this.load().then((token)=>{return resolve(token)},(err)=>{return reject(err);});
        });
    }
    // -------------------------------------------------------------------------------------
    set (name,token) {
        this.__token=token;
        this.__name=name;
    }
    // -------------------------------------------------------------------------------------
    clear() {
        this.__token=null;
        this.__name=null;
    }
    // -------------------------------------------------------------------------------------
}
module.exports=API_TOKEN;
