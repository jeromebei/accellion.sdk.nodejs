class API {
    // -------------------------------------------------------------------------------------
    VERSION = 20;
    CACHE = new (require("../cache.js"))();
    // -------------------------------------------------------------------------------------
    constructor(tokenName) {
        this.request = new (require ("./api.request.js"))(this);
        this.folders = new (require ("./api.folders.js"))(this);
        this.favorites = new (require ("./api.favorites.js"))(this);
        this.roles = new (require ("./api.roles.js"))(this);
        this.token = new (require ("./api.token.js"))(this,tokenName);
    }    
    // -------------------------------------------------------------------------------------
}

module.exports = API;