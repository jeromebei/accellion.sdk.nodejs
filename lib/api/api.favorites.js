class API_FAVORITES {
    // -------------------------------------------------------------------------------------
    constructor(API) {
        this.API=API;
     }
    // -------------------------------------------------------------------------------------
    list (options) {
      return new Promise((resolve, reject) =>{
        this.API.request.get("/favorites",this.API.request.__query(options)).then((response)=>{
          if (!response.data || !response.data.data) return reject("no data");
          return resolve(response.data.data);
        },(err)=>{
            return reject(err);
        });
      });      
    }    
    // -------------------------------------------------------------------------------------
}
module.exports=API_FAVORITES;
