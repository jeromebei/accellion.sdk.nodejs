const express = require('express');
const querystring = require('querystring');
const https = require('https');
const fs = require('fs');
const path = require('path');
const prompt = require('prompt');
const open = require('open');
const keytar = require('keytar');

const API = require('./api/api.js');

var PROTOCOL="https://";
var LOCALPORT=8080;
var REDIRECT_URI=`http://localhost:${LOCALPORT}/oauth`;
var TOKENNAME="*default*";
var TOKENSERVICE="accellion.token";
var SERVER=null;
var CLIENT_ID=null;
var CLIENT_SECRET=null;

var api = new API();
var app = express();

console.log("");
console.log("---------------------------");
console.log("Accellion API Token Creator");
console.log("---------------------------");
console.log("");
console.log(`Using API Version ${api.VERSION}`);
console.log("");
console.log("In order to create a token, you will need to have access to an API-enabled Accellion server.");
console.log("Create a custom application in the Accellion admin interface, use 'Authorization Code' for");
console.log("the authentication flow, and set the redirect URI to 'http://localhost:8080/oauth'.");
console.log("");
console.log("Tokens will be stored in your secure credential manager / keychain, so they will only be");
console.log("accessible by the currently logged on user account.");
console.log("");

var schema = {
    properties: {
        CLIENT_ID :    {description: `Custom Application Client ID`,type:'string',required:true},
        CLIENT_SECRET: {description: `Custom Application Secret`,type:'string',required:true},
        SERVER :       {description: `Accellion Server FQDN`,type:'string',required:true},
        TOKENNAME :    {description: `Token Name`,type:'string',required:true, default: TOKENNAME},
    }
};

prompt.start();
prompt.message="";
prompt.delimiter=":";
prompt.get(schema, function (err, result) {
    if (err) process.exit(0);
    SERVER=result.SERVER;
    CLIENT_ID=result.CLIENT_ID;        
    CLIENT_SECRET=result.CLIENT_SECRET;
    TOKENNAME=result.TOKENNAME;

    console.log("");
    console.log(`Accellion Server: ${PROTOCOL}${SERVER}`);
    console.log(`Client ID:        ${CLIENT_ID}`);
    console.log(`Secret:           ${CLIENT_SECRET}`);
    console.log(`Token Name:       ${TOKENNAME}`);    
    console.log("");
    prompt.delimiter="?";
    prompt.get({properties:{OK:{description:"Is this correct (y/n)",type:'string',required:true}}}, function (err, result2) {
        if (result2.OK.toLocaleLowerCase().startsWith("y")) {
            console.log("");
            execute();
        }
    });
});

function execute() {
    app.get('/login', function (req, res) {
        var authUri = PROTOCOL+SERVER + "/oauth/authorize?client_id=" + CLIENT_ID + "&response_type=code&scope=&redirect_uri=" + encodeURIComponent(REDIRECT_URI);
        res.redirect(authUri);
    });
    app.get('/oauth', function (req, res) {
        var code = req.query.code;
        var form = {
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            grant_type: "authorization_code",
            redirect_uri: encodeURIComponent(REDIRECT_URI),
            code: code
        };
        var formData=querystring.stringify(form);
        var options = {
          hostname: SERVER,
          port: 443,
          path: '/oauth/token',
          method: 'POST',
          headers: {'Content-Type': 'application/x-www-form-urlencoded','Content-Length': formData.length}
        };
        const req2 = https.request(options, res2 => {
          res2.on('data', d => {
            var tokenData = JSON.parse(d.toString('utf8'));
            tokenData.apiVersion=api.API_VERSION;
            tokenData.clientId=CLIENT_ID;
            tokenData.clientSecret=CLIENT_SECRET;
            tokenData.server=PROTOCOL+SERVER;
            keytar.setPassword(TOKENSERVICE,TOKENNAME,JSON.stringify(tokenData)).then(()=>{
                console.log(`Token stored in keychain ${TOKENSERVICE}, account: ${TOKENNAME}`);
                console.log(`All done.`);
                console.log("");
                res.send("Token retrieved and stored. You can now close this browser window.");
                process.exit(0);
                });
          });
        });
        req2.on('error', (e) => {
          console.error(e);
        });
      
        req2.write(formData);
        req2.end();
    });
      
      
    app.listen(LOCALPORT, function () {
        console.log(`Please open a web browser with the following URL:`);
        console.log("");
        console.log(`   http://localhost:${LOCALPORT}/login`);
        console.log("");
        console.log("Log in with your Accellion user account and grant access to the custom application.");
        console.log("");
        open(`http://localhost:${LOCALPORT}/login`);
    });
}

