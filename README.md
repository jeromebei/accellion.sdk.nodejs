# Accellion API Usage Examples
A collection of [Node.JS](https://nodejs.org/) based [Accellion](https://www.accellion.com) API usage examples. 

## Prerequisites
In order to run the examples, you will need to install [Node.JS](https://nodejs.org/) on your system and have backend access to an [Accellion](https://www.accellion.com) Content Firewall that is licensed with the REST Api option. 

## Installation
Download / clone the repository, then run the following command inside the download folder:

    npm install

## Configuration
Before running the example code, you will need to configure a custom application in the backend of the [Accellion](https://www.accellion.com) Content Firewall. Please refer to the Accellion Administrator's Guide, section "Custom Applications" to learn how to create a custom application. 

The custom application needs to have the "Authorization Code" authentication flow enabled and the Redirect URI needs to point to "http://localhost:8080/oauth".

Please remember the Client ID and Secret once the custom application has been created in Accellion, you will need it to create a new token.

To generate a token to be used by the examples, open a command prompt and CD into the repositories' root folder. Then run the following command:

    node lib/create-token.js

After you have entered the Client ID, Secret, Server FQDN and an optional Token Name, a local server instance will start on port 8080. Your default browser should open and allow you to login to the Accellion server. If it doesn't, manually open your browser and navigate to "http://localhost:8080/login".

Login with your Accellion credentials and grant access to your custom application. Once completed, the token retrieved from the Accellion backend will be stored in your credential manager / key chain.

All of the examples accessing the Accellion Rest Api will use the default token (named '**default**') from your credential manager / key chain to authenticate; hence make sure the token exists before running any of the example code.

## Getting started

The /lib/api/api.js library encapsulates some of the Accellion API endpoints and simplifies https REST requests towards the Accellion server instance. It makes use of the token(s) created with the /lib/create-token.js script stored in your credential manager / keychain.

Check the /examples folder to see how the api.js library can be used.

## Example code documentation

- [Activities](./examples/activities/)
- [Admin](./examples/admin/)
- [Approval Workflow](./examples/approval_workflow/)
- [Email](./examples/email/)
- [User](./examples/user/)
- [File](./examples/file/)
- [Epic API](./examples/epic/)

## Making rest api requests  

The /lib/api/api.js library allows you to simplify the common rest api requests (get, delete, head, options, post, patch, put) and also has wrapper methods for file uploads and downloads.

### Sample rest api request

The following example uses the API library in order to retrieve information about the current user. Note that it uses the default token, so you will have to create a token with the /lib/create-token.js script first.

~~~javascript
const API = require('./lib/api/api.js');
const api = new API();
api.request.get("/users/me").then((response) => {
    return console.log(response.data);
});
~~~

## Downloading content
The API library contains a convenience method to download files. It will create a write stream for the file to a local folder and return a promise. You will have to specify the endpoint, file object (containing a file id and name) as well as a folder path to download to.

### Example

~~~javascript
var api = new API();
api.request.download(`/files/${file.id}/content`,file, folderPath).then(()=>{
    console.log(`file ${file.name} downloaded`);
});
~~~

## Uploading content

The API library contains 2 convenience methods to upload local files to an Accellion instance:

### api.request.upload
This method will upload a file in a single API endpoint call using a multipart/form-data content type. It allows you to specify the endpoint, a local file path / name and returns a promise. This method is limited by the amount of data you can send in a single POST request.

### Example

~~~javascript
var api = new API();
api.request.upload(`/folders/${folderId}/actions/file?returnEntity=true`, "file.ext").then((response)=>{
    console.log("done");
},(err)=>{
    console.error(err);
});
~~~

### api.request.uploadChunks
This method will upload content in chunks, allowing you to send large files with no file size limit. You can cancel an upload in process at any time and provide a callback function that will give you the progress percentage. Also, you have the ability to define the individual chunk size.

The uploadChunks method uses the /folders/${folderId}/actions API endpoint, so you will have to provide the folder ID to initiate a chunked upload. At this point, file chunks are uploaded single-threaded (one by one). To cancel an upload in progress, simply call: 
~~~javascript
    api.request.cancelUpload("<uploading file>");
~~~

### Example
~~~javascript
api.request.uploadChunks(folderId, "image.jpg",4096,(progress)=>{
    console.log(`upload progress: ${progress}`);
}).then((response)=>{
    console.log(`done`);
},(err)=>{
    console.error(err);
});
~~~

## Using multiple tokens

All of the example code in /examples initializes an API instance with the default token from your credential manager / key chain. 

![Structure](https://bitbucket.org/jeromebei/accellion.sdk.nodejs/raw/HEAD/assets/token.png)

In case you need to access multiple Accellion servers or run code with different user accounts, you can create new tokens with different names using the create-token.js script. You can then instantiate the API like so:
~~~javascript
    const api = new API("<your-token-name>");
    //your code
~~~
Instantiating the API will not load the token immediately, instead it will be loaded by api.token.get(), which ultimately calls api.token.load(). If you need to test a token for existence, you can use the following code:

~~~javascript
    const api = new API();
    api.token.test("<your-token-name>").then((token)=>{
        //token exists
    },(err)=>{
        //token not found
    });
~~~
You should not change the token within the same API instance; instead, if you need a to use a new / different token, create a new API instance with that token name.

## Disclaimer 

THIS SAMPLE CODE IS PROVIDED “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) SUSTAINED BY YOU OR A THIRD PARTY, HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SAMPLE CODE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.