const API = require("../lib/api/api.js");

test('load default token', done => {
    var api = new API();
    api.token.load().then((token)=>{
        expect(token).not.toBeNull();
        done();
    },(err)=>{
        done(err);
    });
});

test('load inexistent token', () => {
    var api = new API('DOESNOTEXIST');
    return expect(api.token.load()).rejects.toMatch('token not found: DOESNOTEXIST');
});

test('test default token', done => {
    var api = new API();
    api.token.test("*default*").then((token)=>{
        return done();
    },(err)=>{
        return done(err);
    });
});

test('endpoint /users/aliveToken', done => {
    var api = new API();
    api.request.put("/users/aliveToken",null).then((response)=>{
        expect (response.status).toBe(200);
        done();
    },(error)=>{
        done(error);
    });
});

