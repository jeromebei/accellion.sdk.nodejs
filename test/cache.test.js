const Cache = require ("../lib/cache.js");

var cache = new Cache();
for (var i=0; i< 100; i++) cache.set("TestCache",i,`This is cache entry ${i}`);

test('looks at cache entry 67', () => {
    expect(cache.find("TestCache",67)).toBe("This is cache entry 67");
});