const API = require("../lib/api/api.js");

test('/roles', done => {
    var api = new API();
    api.roles.list().then((data)=>{
        expect (data.length).toBeGreaterThanOrEqual(0);
        done();
    },(error)=>{
        done(error);
    });
});
