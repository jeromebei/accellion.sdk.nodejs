const API = require("../lib/api/api.js");

test('/users/me', done => {
    var api = new API();
    api.request.get("/users/me").then((response)=>{
        expect (response.status).toBe(200);
        done();
    },(error)=>{
        done(error);
    });
});
