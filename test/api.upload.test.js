const path = require('path');
const API = require("../lib/api/api.js");

test('upload-file', done => {
    var api = new API();
    api.request.get("/users/me").then((response)=>{
        var userObj=response.data;
        api.request.upload("/folders/" + userObj.mydirId + "/actions/file?returnEntity=true", path.join(__dirname,"report.pdf")).then((response)=>{
            return done();
        },(err)=>{
            return done(err);
        });
    },(err)=>{
        return done(err);
    });
},20000);
