const API = require("../lib/api/api.js");

test('/folders/list', done => {
    var api = new API();
    api.folders.list().then((data)=>{
        expect (data.length).toBeGreaterThanOrEqual(0);
        done();
    },(error)=>{
        done(error);
    });
});
