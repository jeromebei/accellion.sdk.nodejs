const Threads = require ("../lib/threads.js");

var pool = Threads.createPool("pool",5);
pool.setSize(2);

test('returns the pool size', () => {
    expect(pool.maxThreads).toBe(2);
});
test('returns the pool name', () => {
    expect(pool.name).toBe("pool");
});

for (var i=0; i< 10; i++) pool.add((thread,context,done) => {
    return done(thread,null);
},{message: `This is execution ${i}`});

test('thread execution done', done => {
    pool.run((pool)=>{
        expect(pool.state).toBe(Threads.STATE.STOPPED);
        done();
    });
});