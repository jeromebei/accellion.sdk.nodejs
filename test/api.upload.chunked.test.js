const path = require('path');
const API = require("../lib/api/api.js");

test('upload-file-chunked', done => {
    var api = new API();
    api.request.get("/users/me").then((response)=>{
        var userObj=response.data;
        api.request.uploadChunks(userObj.mydirId, path.join(__dirname,"image.jpg"),4096,(progress)=>{
            console.log(`upload progress: ${progress}`);
        }).then((response)=>{
            return done();
        },(err)=>{
            return done(err);
        });
    },(err)=>{
        return done(err);
    });
},30000);
